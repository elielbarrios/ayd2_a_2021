'use strict';

require('dotenv').config();

var logger = require('morgan');
const express = require('express');
const port = process.env.PORT || 80;
const path = require('path');

var principal = require('./src/routes/routes')


const app = express();

app.use(logger('dev'))


//AGREGAMOS LAS DOS LINEAS SIGUIENTES

//app.use(express.urlencoded({ extended: false }));
//app.use(express.json());


app.use('/principal', principal);

app.get('/', function(req, res) {
    res.send('RUTA PRUEBA');
  });


app.listen(port, function() {
    console.log('Servidor corriendo en el puerto ' + port);
});