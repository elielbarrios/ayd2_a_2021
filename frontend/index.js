'use strict';

require('dotenv').config();

var logger = require('morgan');
const express = require('express');
const port = process.env.PORT || 80;
const path = require('path');

var principal = require('./src/routes/routes')


const app = express();

app.use(logger('dev'))

app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, '/src/views'));
app.use(express.static(path.join(__dirname, '/src/public')));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.use('/', principal);



app.listen(port, function() {
    console.log('Servidor corriendo en el puerto ' + port);
});